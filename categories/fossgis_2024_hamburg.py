category = {
    "title": "Hummel Hummel",
    "tasks": {
        100: {
            "task": "Der zweitgrößte Fluss Hamburgs",
            "solution": "Alster",
            "reward": 100,
            "hint": "namensgebend für leicht alkoholisches Getränk",
        },
        200: {
            "task": "80.000m² in Hamburg dringend benötigte Büroflächen sollte dieser Neubau in der HafenCity bereitstellen",
            "solution": "Elbtower",
            "reward": 200,
            "hint": "nicht ganz fertig geworden",
        },
        300: {
            "task": "Die Herstellung langer Schiffstaue war namensgebend für diese Straße",
            "solution": "Reeperbahn",
            "reward": 300,
            "hint": "kiez, große freiheit, nachts um halb eins",
        },
        400: {
            "task": "Etwa 120 km westnordwestlich entfernt von Hamburg liegt dieser Stadtteil",
            "solution": "Neuwerk",
            "reward": 400,
            "hint": "und hamburgs ältestes Bauwerk, ein Leuchtturm. Der stadtteil klingt gar nicht so ... alt"
        },
        500: {
            "task": "Dieser deutschlandweit einzigartige Umzugshelfer sorgt jeden November für Aufmerksamkeit",
            "solution": "image:categories/media/fossgis_2024_hamburg/schwanenvater.webp",
            "reward": 500,
        },
    }
}
