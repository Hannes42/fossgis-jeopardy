# via Janine Raatz
category = {
    "title": "Nasse Grenzen",
    "tasks": {
        100: {
            "task": "image:categories/media/fossgis_2024_nassegrenzen/rhein.jpg",
            "solution": "Rhein",
            "reward": 100,
            "hint": "Grenze zwischen Deutschland und der Schweiz"
        },
        200: {
            "task": "So nennt man eine politische Grenze, welche durch Wasser definiert ist",
            "solution": "Blaue Grenze",
            "reward": 200,
        },
        300: {
            "task": "Dieses Land hat mit 202.080 km die längste Küstenlänge der Welt",
            "solution": "Kanada",
            "reward": 300,
            "hint": "2 Indonesien 54.716 km, 3 Grönland 44.087 km; Grenzt an drei Ozeane (Atlantischer, Pazifischer, Arktischer)",
        },
        400: {
            "task": "image:categories/media/fossgis_2024_nassegrenzen/kongo.jpg",
            "solution": "Kongo",
            "reward": 400,
            "hint": "zweit größter Fluss Afrikas, 1 Nil, 2 Niger; Grenzfluß zwischen zwei Ländern mit sehr ähnlichem Namen",
        },
        500: {
            "task": "image:categories/media/fossgis_2024_nassegrenzen/entenschnabel.jpg",
            "solution": "Die deutsche Ausschließliche Wirtschaftszone",
            "reward": 500,
            "hint": "Entenschnabel",
        },
    }
}
