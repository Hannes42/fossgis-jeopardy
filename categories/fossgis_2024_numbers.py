category = {
    "title": "Ich möchte gerne Zahlen",
    "tasks": {
        100: {
            "task": "3.36",
            "solution": "Die aktuelle QGIS-Version",
            "reward": 100,
        },
        200: {
            "task": "So viele Geometrietypen unterstützt das GeoJSON-Format",
            "solution": "Sieben",
            "reward": 200,
        },
        300: {
            "task": "In dieser Dimensionszahl liegen Geländemodelle üblicherweise vor",
            "solution": "2.5D",
            "reward": 300,
        },
        400: {
            "task": "Diese Versionsnummer hat GRASS GIS erreicht",
            "solution": "8(.3.2)",
            "reward": 400,
        },
        500: {
            "task": "So viele Kilometer umfasst der durchschnittliche Erdradius",
            "solution": "6371 km",
            "reward": 500,
        },
    }
}
