category = {
    "title": "Test Tasks",
    "tasks": {
        100: {
            "task": "image:https://www.gravatar.com/avatar/236b6fb07d9828c9b5b4a1d8f40a7367?s=64&d=identicon&r=PG&f=1", #/home/hannes/kram/0ms7u5wed6f01.jpg",
            "solution": "solution1",
            "reward": 100,
            "winner": "Caroline",  # TODO document
        },
        200: {
            "task": "<h1>HTML h1 task2</h1>",
            "solution": "<h1>solution2</h1>",
            "reward": 200,
            "winner": False,  # TODO document
        },
        300: {
            "task": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla id sem id leo fringilla imperdiet. In eget magna vel massa imperdiet consequat.",
            "solution": "<div>solution3</div>",
            "reward": 300,
        },
        400: {
            "task": "audio:https://upload.wikimedia.org/wikipedia/commons/8/8f/Test_mp3_opus_16kbps.wav", #sounds/sfxr coin.wav",
            "solution": "solution4",
            "reward": 400,
        },
        500: {
            "task": "video:https://archive.org/download/BigBuckBunny_124/Content/big_buck_bunny_720p_surround.mp4", #/mnt/earx/torrents/Bernd das Brot - Nachtschleife Call-In [0osX2hVyUmc].webm",
            "solution": "solution5",
            "reward": 500,
        }
    }
}
# TODO better structure where we dont have the reward twice (as key AND value)?
# TODO document format