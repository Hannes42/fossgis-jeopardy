category = {
    "title": "Aus Punkten mach ...",
    "tasks": {
        100: {
            "task": "image:points to something/buffer.png",
            "solution": "Puffer",
            "reward": 100,
        },
        200: {
            "task": "image:points to something/extent.png",
            "solution": "Minimal umschließendes Rechteck / Ausdehnung",
            "reward": 200,
        },
        300: {
            "task": "image:points to something/voronoi polygons.png",
            "solution": "Voronoi-Polygone",
            "reward": 300,
        },
        400: {
            "task": "image:points to something/heatmap kde.png",
            "solution": "KDE-Interpolation / Heatmap",
            "reward": 400,
        },
        500: {
            "task": "image:points to something/delaunay triangulation.png",
            "solution": "Delaunay-Triangulation",
            "reward": 500,
        },
    }
}
