category = {
    "title": "Form & Formate",
    "tasks": {
        100: {
            "task": "Dieses Geodatenformat wird für den Datenaustausch bei OGC API Features verwendet",
            "solution": "GeoJSON",
            "reward": 100,
            "hint": "zb OGC API Features, Freitag der 13. im Wal, Jason",
        },
        200: {
            "task": "<blockquote><em>Sammlung von Formen, vielfältig und bunt, welcher Geometrietyp bringt sie in einen Bund?</em></blockquote><br />... sprach die 'KI'",
            "solution": "GeometryCollection",
            "reward": 200,
        },
        300: {
            "task": "Von den dateibasierten Datenbankformaten GeoPackage, FileGeoDatabase und SpatiaLite basiert nur dieses nicht auf SQLite",
            "solution": "FileGeoDatabase",
            "reward": 300,
        },
        400: {
            "task": "Dieses moderne, spaltenbasierte Dateiformat bewegt sich erst seit kurzem auf dem Geodaten-Parkett",
            "solution": "GeoParquet",
            "reward": 400,
            "hint": "aussprache?",
        },
        500: {
            "task": "Für Kartendaten dieser Art wird meist das&nbsp;S-57-Format verwendet",
            "solution": "(Elektronische) Seekarten",
            "reward": 500,
            "hint": "IHO standard, zb bathymetrische Informationen, Sektorenfeuer, hydrography",
        }
    }
}
