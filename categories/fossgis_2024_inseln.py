# via marc tobias, inkl bonus/backup (s.u.)
category = {
    "title": "Inseln",
    "tasks": {
        100: {
            "task": "Diese Insel ist die größte Deutschlands",
            "solution": "Rügen",
            "reward": 100,
            "hint": "926km², ich will euch jetzt nicht rügen aber das könnte man schon wissen",
        },
        200: {
            "task": "1995 haben Punks Chaostage auf dieser Insel ausgerufen",
            "solution": "Sylt",
            "reward": 200,
            "hint": "Damals wegen dem Schönes-Wochenede-Ticket (15 DM), also ähnlich dem 9-Euro-Ticket in 2022, und kann man erraten.",
        },
        300: {
            "task": "image:categories/media/fossgis_2024_inseln/insel.png",
            "solution": "Juist",
            "reward": 300,
            "hint": "17 Kilometer lang, 500-900m breit",
        },
        400: {
            "task": "Diese Ostseeinsel teilt sich Deutschland mit einem Nachbarland",
            "solution": "Usedom",
            "reward": 400,
            "hint": "Polen",
        },
        500: {
            "task": "Dieses Land hat 1890 Helgoland an das Deutsche Reich abgetreten",
            "solution": "Großbritannien / Vereinigtes Königreich  ",
            "reward": 500,
            "hint": "sog. 'Helgoland-Sansibar-Vertrag' (nein, nicht so wirklich Tausch für Sansibar afaik), gehört zum Kreis Pinneberg, Fuselfelsen",
        },
    }
}

"""
Bonus/Backup

* Diese Insel ist die einzige deutsche Hochseeinsel
=> Helgoland  


* So heissen kleine, nur wenig geschützte Marschinseln im 
nordfriesischen Wattenmeer
 [...]  


* Ein 2km breiter Meeresarm verbindet Stralsund mit dieser
Insel
=> Rügen  
# https://de.wikipedia.org/wiki/Strelasund
# Auch der Rügendamm und Rügenbrücke aber das wäre vielleicht zu
# offensichtlich. Eigentlich muss man nur wissen, dass Stralsund
# direkt an Rügen grenzt.


* Soviele der 7 bewohnten Ostfriesischen Inseln haben einen
Flugplatz
=> 6  
# Nur Spiekeroog hat keinen
"""
