category = {
    "title": "Abk.",
    "tasks": {
        100: {
            "task": "OSM",
            "solution": "OpenStreetMap",
            "reward": 100,
        },
        200: {
            "task": "KonGeoS",
            "solution": "Konferenz der Geodäsie-Studierenden der Hochschulen im DACH-Raum",
            "reward": 200,
        },
        300: {
            "task": "NFDI",
            "solution": "Nationale Forschungsdateninfrastruktur",
            "reward": 300,
        },
        400: {
            "task": "AdV",
            "solution": "Arbeitsgemeinschaft der Vermessungsverwaltungen der Länder der Bundesrepublik Deutschland",
            "reward": 400,
            "hint": "publikumspreis für vollen namen"
        },
        500: {
            "task": "STAC",
            "solution": "SpatioTemporal Asset Catalogs",
            "reward": 500,
        },
    }
}
