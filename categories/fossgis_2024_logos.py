
category = {
    "title": "Na? Logo?",
    "tasks": {
        100: {
            "task": "image:categories/media/fossgis_2024_logos/LOGO_FOSSGIS24_RGB_300dpi_censored.png",
            "solution": "image:categories/media/fossgis_2024_logos/LOGO_FOSSGIS24_RGB_300dpi.png",
            "reward": 100,
        },
        200: {
            "task": "image:categories/media/fossgis_2024_logos/gis.stackexchange.com_censored.svg",
            "solution": "image:categories/media/fossgis_2024_logos/gis.stackexchange.com.svg",
            "reward": 200,
        },
        300: {
            "task": "image:categories/media/fossgis_2024_logos/basemap.de_censored.svg",
            "solution": "image:categories/media/fossgis_2024_logos/basemap.de.svg",
            "reward": 300,
        },
        400: {
            "task": "image:categories/media/fossgis_2024_logos/STAC-01_censored.png",
            "solution": "image:categories/media/fossgis_2024_logos/STAC-01.png",
            "reward": 400,
        },
        500: {
            "task": "image:categories/media/fossgis_2024_logos/logo_EuroCarto2024_TEST-01_censored.svg",
            "solution": "image:categories/media/fossgis_2024_logos/logo_EuroCarto2024_TEST-01.svg",
            "reward": 500,
        },
    }
}
