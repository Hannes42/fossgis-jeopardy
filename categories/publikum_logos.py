category = {
    "title": "Logos",
    "tasks": {
        100: {
            "task": "image:logos/FOSSGIS_Logo edited.svg",
            "solution": "image:logos/FOSSGIS_Logo.svg",
            "reward": 100,
        },
        200: {
            "task": "image:logos/osm.svg",
            "solution": "image:logos/osm_solution.png",
            "reward": 200,
        },
        300: {
            "task": "image:logos/openlayers.svg",
            "solution": "image:logos/openlayers_solution.png",
            "reward": 300,
        },
        400: {
            "task": "image:logos/osgeolive.png",
            "solution": "image:logos/osgeolive_solution.png",
            "reward": 400,
        },
        500: {
            "task": "image:logos/GDALLogoColor edited.svg",
            "solution": "image:logos/GDALLogoColor.svg",
            "reward": 500,
        },
    }
}
