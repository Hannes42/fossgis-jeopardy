category = {
    "title": "FOSSGIS",
    "tasks": {
        100: {
            "task": "Das dritte S in FOSSGIS steht dafür",
            "solution": "Systeme",
            "reward": 100,
        },
        200: {
            "task": "Der FOSSGIS e. V. ist ein Local Chapter dieser gemeinnützigen Organisation",
            "solution": "OSGeo & OSMF",
            "reward": 200,
            "hint": "Zwei mögliche Antworten",
        },
        300: {
            "task": "In diesem Hotel findet mindestens einmal jährlich ein Arbeitstreffen rund um FOSSGIS und OSM statt",
            "solution": "Linuxhotel",
            "reward": 300,
            "hint": "Essen",
        },
        400: {
            "task": "In dieser Stadt findet die diesjährige FOSS4G Europe statt",
            "solution": "Tartu",
            "reward": 400,
            "hint": "Estland, nicht Tallinn, zweitgrößte Stadt, deutscher Name Dorpat, eine der drei Europäischen Kulturhauptstädte 2024",
        },
        500: {
            "task": "In diesem Jahr fand die erste FOSSGIS-Konferenz statt",
            "solution": "2003",
            "reward": 500,
            "hint": "in Bonn, vorher UMN MapServer Anwenderkonferenz, 350 Teilnehmende, Jörg hatte 2003 gesagt aber das war noch keine FOSSGIS",
        },
    }
}
