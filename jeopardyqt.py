import glob
import http.server
import logging
import sys
import random
import socketserver
import threading

from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QApplication

from game import Game
from main_window import MainWindow
from config import Config


logging.basicConfig(
    format='%(asctime)s,%(msecs)03d %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s',
    datefmt='%Y-%m-%d:%H:%M:%S',
    level=logging.DEBUG
)


if __name__ == "__main__":
    app = QApplication([])

    # TODO: Font Ubuntu is not available on all Linux and Mac OSes
    font = QFont("Ubuntu")
    font.setStyleHint(QFont.SansSerif)
    app.setFont(font)

    game_states_dir = "/tmp"  # TODO put in config?
    available_game_states = glob.glob(f"{game_states_dir}/jeopardy_*.pickle")
    if available_game_states:
        # TODO present list of available states in a dialog?
        game = Game.from_latest_pickle()
    else:
        # game setup, TODO provide dialogs for setup
        players = []
        for name, color in Config.player_names_and_colors:
            players.append({"name": name, "score": 0, "color": color})

        game = Game(
            players=players,
            categories=Config.categories,
            active_player=random.choice(players),  # TODO actually use this ;D
        )

        #game.export_solutions()

    window = MainWindow(game)
    window.show()

    # Start webserver in a separate thread
    Handler = http.server.SimpleHTTPRequestHandler
    with socketserver.TCPServer((Config.server, Config.port), Handler) as httpd:
        logging.debug("serving at port %s", Config.port)
        threading.Thread(target=httpd.serve_forever, daemon=True).start()

        exit_code = app.exec()

        # we back!
        game.stop_audio()
        httpd.shutdown()
        sys.exit()
