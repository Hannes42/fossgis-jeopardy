import glob
import logging
import pickle
import random
from datetime import datetime
from typing import Iterable, Union

from PyQt5.QtCore import QObject, pyqtSignal, QUrl, QFileInfo
from PyQt5.QtMultimedia import QMediaPlayer, QMediaContent

from config import Config
from utils import validate_category, validate_players


class Game(QObject):
    playerRewarded = pyqtSignal([int, int])  # player_index and reward (+-x points)
    activePlayerChanged = pyqtSignal()  # player_index
    taskEnded = pyqtSignal(bool)  # true/false depending on the players' success in answering/solving

    def __init__(self, players: Iterable[dict], categories: Iterable[dict], active_player: Union[dict, None] = None):
        super().__init__()

        validate_players(players)  # raises Exception on error
        self.players = players

        for category in categories:
            validate_category(category)  # raises Exception on error
        self.categories = categories

        self.active_player = active_player  # TODO if already set, use for highlighting player to choose task
                                            # TODO PS: that is not implemented at all currently btw ;)
        self.current_task: Union[dict, None] = None

        self.audio_player = QMediaPlayer()
        # note that if sound is played in a QWebEngineView, this is not controlled by QMediaPlayer
        self.port = Config.port
        self.server = Config.server

    def play_music(self) -> None:
        sound_url = QUrl.fromLocalFile(QFileInfo(Config.music["thinking"]).absoluteFilePath())
        self.play_sound(sound_url)

    def stop_audio(self) -> None:
        logging.debug("Stopping audio")
        self.audio_player.stop()

    def play_sound(self, url: str) -> None:
        self.audio_player.stop()
        self.audio_player.setMedia(QMediaContent(url))
        self.audio_player.play()

    def play_fail_sound(self) -> None:
        random_fail_sound = random.choice(Config.sounds["fail"])
        url = QUrl.fromLocalFile(QFileInfo(random_fail_sound).absoluteFilePath())
        self.play_sound(url)

    def play_task_fail_sound(self) -> None:
        random_fail_sound = random.choice(Config.sounds["task_fail"])
        url = QUrl.fromLocalFile(QFileInfo(random_fail_sound).absoluteFilePath())
        self.play_sound(url)

    def play_success_sound(self) -> None:
        random_success_sound = random.choice(Config.sounds["success"])
        url = QUrl.fromLocalFile(QFileInfo(random_success_sound).absoluteFilePath())
        self.play_sound(url)

    def get_player_index(self, player: dict) -> Union[int, None]:
        # TODO lol..., andere struktur nötig!
        for i, player_ in enumerate(self.players):
            if player_ is player:
                return i
        return None

    def set_active_player(self, player_index: Union[int, None]) -> None:
        if player_index is not None:
            self.stop_audio()  # stop the music
            self.active_player = self.players[player_index]
        else:
            self.active_player = None
        self.activePlayerChanged.emit()

    def update_player_score(self, active_player: dict, points: int) -> None:
        logging.debug(f"update_player_score({active_player}, {points})")
        self.active_player["score"] += points
        self.playerRewarded.emit(self.get_player_index(active_player), points)

    def fail_active_player(self) -> None:
        logging.debug("fail_active_player")
        self.play_fail_sound()
        self.update_player_score(self.active_player, -self.current_task["reward"])
        self.set_active_player(None)

    def reward_active_player(self) -> None:
        """TODO docstring

        Note: Mutates the task to store the winner!
        """
        logging.debug("reward_active_player")
        self.play_success_sound()
        self.update_player_score(self.active_player, self.current_task["reward"])
        self.current_task["winner"] = self.active_player["name"]
        self.current_task = None
        self.taskEnded.emit(True)

    def fail_current_task(self) -> None:
        """TODO docstring

        Note: Mutates the task to store the failed status!
        """
        logging.debug("fail_current_task")
        self.play_task_fail_sound()
        self.current_task["winner"] = False
        self.current_task = None
        self.set_active_player(None)
        self.taskEnded.emit(False)

    @property
    def player_ranking(self) -> list[dict]:
        """Returns the players sorted by descending score."""
        return sorted(self.players, key=lambda player: player["score"], reverse=True)

    def export_solutions(self):
        # export solutions for moderation, todo make this a button or something? fix html if possible?
        with open("/tmp/solutions.md", "w") as sink:
            for category in self.categories:
                sink.write(f"# {category['title']}\n")
                for reward, task in category["tasks"].items():
                    sink.write(f"- {reward}: {task['solution']}\n")
                    hint = task.get("hint")
                    if hint:
                        sink.write(f"    - Tipps: {hint}\n")

    def save_as_pickle(self) -> None:
        """TODO"""
        assert not self.current_task

        game_state = {
            "players": self.players,
            "categories": self.categories,
            "active_player": self.active_player,
        }

        now = datetime.now().strftime("%Y%m%d-%H%M%S")
        with open(f"/tmp/jeopardy_{now}.pickle", "wb") as sink:  # TODO save in non-temporary path to be safer! ;D
            pickle.dump(game_state, sink)
        logging.debug(f"Saved game state: {now}.")

    @classmethod
    def from_pickle(cls, path):
        """Creates a Game instance from data saved via Game.save_as_pickle()

        yeah yeah not safe etc
        TODO better restructure the categories so only string keys are used (stupid json has no int keys...)

        Args:
            path: Pickle file to load the stored game state from
        """
        with open(path, "rb") as source:
            game_state = pickle.load(source)
            game = cls(
                players=game_state["players"],
                categories=game_state["categories"],
                active_player=game_state["active_player"],
            )
            logging.debug(f"Loaded game state: {path}.")
            return game

    @classmethod
    def from_latest_pickle(cls):
        """Creates a Game instance from the latest available save in /tmp/

        Note: Uses the *filename* to determine temporal order.
        """
        game_state_files = glob.glob('/tmp/jeopardy_*.pickle')  # TODO adjust to save path above if necessary
        latest_game_state = max(game_state_files)  # assuming they are nicely timestamped
        return cls.from_pickle(latest_game_state)
