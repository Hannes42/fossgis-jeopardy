from typing import Iterable


def validate_category(category: dict):
    """Validates the category according to some rules.

    Raises:
        Exception if invalid
    """

    # validate category
    if set(category.keys()) != {"title", "tasks"}:
        raise Exception(f"Category is must have title and tasks but has {set(category.keys())}")

    # validate tasks
    tasks = category["tasks"]
    if set(tasks.keys()) != {100, 200, 300, 400, 500}:
        raise Exception(
            f"Category must have tasks for 100, 200, 300, 400 and 500 points each but has {tasks.keys()}")
    for task_reward, task in tasks.items():
        if not set(task.keys()) <= {"task", "solution", "reward", "winner", "hint"}:
            raise Exception(
                f"Task is must have task, solution, reward and optionally winner but has {set(task.keys())}")


def validate_players(players: Iterable[dict]):
    # make sure our player names and colors are unique TODO build something better for this :D
    player_names = [player["name"] for player in players]
    if not len(players) == len(set(player_names)):
        raise ValueError(f"Player names are not unique: {player_names}")
    player_colors = [player["color"] for player in players]
    if not len(players) == len(set(player_colors)):
        raise ValueError(f"Player colors are not unique: {player_colors}")
