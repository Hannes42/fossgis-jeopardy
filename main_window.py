import logging
from typing import Union

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QKeyEvent
from PyQt5.QtWidgets import QMainWindow, QWidget, QStackedLayout, QHBoxLayout, QLabel, QVBoxLayout, QTextBrowser
from game import Game
from tasks_grid import TasksGrid
from task_widget import TaskWidget
from qt_utils import maximize_pointsize_in_widget


class MainWindow(QMainWindow):
    """Dialog with display of categories/tasks, player names+scores and a debug/history log."""

    def __init__(self, game: Game):
        super().__init__()
        self.game = game

        # welcome to the switchboard
        self.game.playerRewarded.connect(self.show_updated_player_score)
        self.game.activePlayerChanged.connect(self.color_app_with_player_color)
        self.game.taskEnded.connect(self.on_task_ended)

        self.setWindowTitle("Jeopardy-Qt")
        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)

        self.tasks_grid_widget = TasksGrid(self.game, parent=self)
        self.stacked_layout = QStackedLayout()
        self.stacked_layout.addWidget(self.tasks_grid_widget)
        # active task widget is dynamically added

        self.player_widgets = []  # list of dicts with name + score
        self.players_layout = QHBoxLayout()
        for player in self.game.players:
            logging.info(f"Add player: {player['name']}")
            player_name_label = QLabel(player["name"])
            player_score_label = QLabel(str(player["score"]))
            font = player_score_label.font()
            font.setPointSize(16)
            player_name_label.setFont(font)
            font.setBold(True)
            font.setPointSize(24)
            player_score_label.setFont(font)
            player_widget = QWidget()
            player_widget.setStyleSheet(f"background-color: {player['color']};")
            player_layout = QVBoxLayout(player_widget)
            player_layout.addWidget(player_name_label)
            player_layout.addWidget(player_score_label)
            self.players_layout.addWidget(player_widget)
            self.player_widgets.append(
                {
                    "widget": player_widget,
                    "name": player_name_label,
                    "score": player_score_label
                }
            )
        # TODO center text

        self.debugging_textbrowser = QTextBrowser()
        self.debugging_textbrowser.setFixedHeight(100)
        self.debugging_textbrowser.setVisible(False)  # toggleable via Ctrl-D

        main_layout = QVBoxLayout()
        main_layout.addLayout(self.stacked_layout)
        main_layout.addLayout(self.players_layout)
        main_layout.addWidget(self.debugging_textbrowser)
        self.central_widget.setLayout(main_layout)

        self.setWindowFlags(Qt.CustomizeWindowHint | Qt.FramelessWindowHint)
        self.showFullScreen()

    def show_updated_player_score(self, player_index: Union[int, None], reward: int) -> None:
        self.debugging_textbrowser.append(f"Player {self.game.active_player['name']} gets {reward} points")
        self.player_widgets[player_index]["score"].setText(str(self.game.active_player["score"]))

    def color_app_with_player_color(self) -> None:
        if self.game.active_player is not None:
            self.debugging_textbrowser.append(f"Active Player changed to {self.game.active_player['name']}")
            self.setStyleSheet(f"QMainWindow {{background-color: {self.game.active_player['color']};}}")
        else:
            self.debugging_textbrowser.append("Active Player changed to None")
            self.setStyleSheet("")

    def on_task_ended(self, success: bool) -> None:
        # update task's button state and indicate which player chooses next task
        if success:
            self.tasks_grid_widget.color_task_button(self.game.active_player['color'])
            self.setStyleSheet(f"QMainWindow {{background-color: {self.game.active_player['color']};}}")
        else:
            self.tasks_grid_widget.disable_task_button()
            self.setStyleSheet("")

        # save game state
        self.game.save_as_pickle()

        # check if game is finished
        # TODO this should really be something in Game instead!?
        game_has_open_tasks = False
        for category in self.game.categories:
            for task in category["tasks"].values():
                if task.get("winner") is None:
                    game_has_open_tasks = True
        if not game_has_open_tasks:
            ranked_players = self.game.player_ranking
            if len(ranked_players) > 1 and ranked_players[0]["score"] == ranked_players[1]["score"]:  # at least two players tie
                self.setStyleSheet("")
                winner_widget = QLabel("Gleichstand!?!")
            else:
                winner_widget = QLabel(f"""<b>{ranked_players[0]["name"]}</b> hat gewonnen!""")
            maximize_pointsize_in_widget(winner_widget)
            self.stacked_layout.addWidget(winner_widget)
            self.stacked_layout.setCurrentWidget(winner_widget)

    def update_ui_after_task(self) -> None:
        self.stacked_layout.setCurrentWidget(self.tasks_grid_widget)

    def show_task(self, task: dict) -> None:
        self.game.set_active_player(None)
        self.game.current_task = task

        self.game.stop_audio()
        if task["task"].startswith("audio:"):
            logging.debug("Playing music because it is an audio task.")
        else:
            self.game.play_music()

        task_widget = TaskWidget(self.game, self)
        self.stacked_layout.addWidget(task_widget)
        self.stacked_layout.setCurrentWidget(task_widget)

    def keyPressEvent(self, event: QKeyEvent) -> None:
        key = event.key()
        modifiers = event.modifiers()
        logging.debug(f"keyPressEvent: {key}")

        if key == Qt.Key_1 and not self.game.active_player:
            self.game.set_active_player(0)
        elif key == Qt.Key_2 and not self.game.active_player:
            self.game.set_active_player(1)
        elif key == Qt.Key_3 and not self.game.active_player:
            self.game.set_active_player(2)
        elif key == Qt.Key_4 and not self.game.active_player:
            self.game.set_active_player(3)
        elif key == Qt.Key_Escape or key == Qt.Key_Q:
            self.game.set_active_player(None)
        elif modifiers == Qt.ControlModifier and key == Qt.Key_Z:
            logging.warning("TODO Trying to undo the last action")
        elif modifiers == Qt.ControlModifier and key == Qt.Key_Y:
            logging.warning("TODO Trying to redo the last action")
        elif modifiers == Qt.ControlModifier and key == Qt.Key_D:
            self.debugging_textbrowser.setVisible(not self.debugging_textbrowser.isVisible())
        else:
            logging.debug(f"Ignoring: {key=}, {modifiers=}")
