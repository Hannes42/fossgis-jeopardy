import logging

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QKeyEvent
from PyQt5.QtWebEngineWidgets import QWebEngineView, QWebEnginePage, QWebEngineSettings  # needs python-pyqt5-webengine
from PyQt5.QtWidgets import QWidget, QVBoxLayout


class TaskWidget(QWidget):

    def __init__(self, game, main_window):
        super().__init__()

        # implicitly delete the widget after use, so that it does not live on forever (e.g. playing audio on and on)
        self.setAttribute(Qt.WA_DeleteOnClose)

        self.game = game
        self.main_window = main_window

        self.server_with_port = f'http://{game.server}:{game.port}'
        self.task_page = self.build_html_page(self.game.current_task["task"], self.server_with_port)
        self.solution_page = self.build_html_page(self.game.current_task["solution"], self.server_with_port)

        self.webengineview = QWebEngineView(self)
        self.webengineview.setPage(self.task_page)

        layout = QVBoxLayout()
        layout.addWidget(self.webengineview)
        self.setLayout(layout)

    @staticmethod
    def build_html_page(content: str, server: str) -> QWebEnginePage:

        css = """
        html, body, div {
           margin: 0px;
           padding: 0px;
           font-family: Ubuntu, sans-serif;
        }
        
        /* https://stackoverflow.com/questions/356809/best-way-to-center-a-div-on-a-page-vertically-and-horizontally */
        #parent {
          display: flex;
          justify-content: center;
          align-items: center;
          height: 100vh;
          width: 100vw;
        }
        
        /* via https://stackoverflow.com/a/48606760 */
        .filling_media {
            position: absolute;
            z-index: -1;
            top: 0;
            left: 0;
            width: 100%; 
            height: 100%;
            object-fit: contain;
            image-rendering: pixelated;  /* good upscale for small icons etc. */
        }
        
        /* dynamically scale text based on the window size */
        /* TODO test https://github.com/adactio/FitText.js ? */
        .big_text {
            font-size: max(5vw, 5vh);  /* naja, geht so... */
            white-space: wrap;
        }
        """

        # TODO support local media https://forum.qt.io/topic/119534/proper-way-to-display-local-images-in-qtwebengineview/9
        # Formats: https://www.chromium.org/audio-video/

        html = f"""
            <style>{css}</style>
            <div id="parent">
                <div id="child">
        """
        if content.startswith("audio:"):
            audio_file = content[len("audio")+1:]
            html += f"<audio src='{server}/{audio_file}' controls autoplay>Not supported?</audio>"
        elif content.startswith("image:"):
            image_file = content[len("image")+1:]
            html += f"<img class='filling_media' src='{server}/{image_file}' />"
        elif content.startswith("video:"):
            video_file = content[len("video")+1:]
            html += f"<video class='filling_media' src='{server}/{video_file}' controls autoplay>Not supported?</video>"  # TODO controls don't show after initial load?
        else:
            html += f"<div class='big_text'>{content}</div>"

        html += """
                </div>
            </div>
        """

        page = QWebEnginePage()
        page.settings().setAttribute(QWebEngineSettings.PlaybackRequiresUserGesture, False)  # allow autoplay
        page.setHtml(html)
        return page

    def keyPressEvent(self, event: QKeyEvent) -> None:
        key = event.key()
        modifiers = event.modifiers()
        logging.debug(f"keyPressEvent: {key}")

        # determine wanted action
        if self.webengineview.page() is self.task_page:
            logging.debug("task page is currently displayed, handling input")
            if key == Qt.Key_1 and not self.game.active_player:  # sudo footswitch -k 1
                self.game.set_active_player(0)
            elif key == Qt.Key_2 and not self.game.active_player:
                self.game.set_active_player(1)
            elif key == Qt.Key_3 and not self.game.active_player:
                self.game.set_active_player(2)
            elif key == Qt.Key_4 and not self.game.active_player:
                self.game.set_active_player(3)
            elif key == Qt.Key_1 and self.game.active_player:
                self.main_window.debugging_textbrowser.append(f"Calm down {self.game.players[0]['name']}, it's {self.game.active_player['name']}'s turn!")
            elif key == Qt.Key_2 and self.game.active_player:
                self.main_window.debugging_textbrowser.append(f"Calm down {self.game.players[1]['name']}, it's {self.game.active_player['name']}'s turn!")
            elif key == Qt.Key_3 and self.game.active_player:
                self.main_window.debugging_textbrowser.append(f"Calm down {self.game.players[2]['name']}, it's {self.game.active_player['name']}'s turn!")
            elif key == Qt.Key_4 and self.game.active_player:
                self.main_window.debugging_textbrowser.append(f"Calm down {self.game.players[3]['name']}, it's {self.game.active_player['name']}'s turn!")
            #elif key == Qt.Key_5:
            #    self.webengineview.setPage(self.solution_page)
            #    del self.task_page  # necessary to stop its process, otherwise audio/video would play in background
            elif key == Qt.Key_Y and self.game.active_player:
                self.webengineview.setPage(self.solution_page)
                self.task_page = None  # necessary to stop its process, otherwise audio/video would play in background
                self.setFocus()  # otherwise focus would get lost to main window
                self.game.reward_active_player()
            elif key == Qt.Key_N and self.game.active_player:
                self.game.fail_active_player()
                self.game.set_active_player(None)
            elif key == Qt.Key_Escape or key == Qt.Key_Q:  # Argh, QWebEngineView does not propagate Escape key to here...
                self.game.set_active_player(None)
                self.task_page = None  # necessary to stop its process, otherwise audio/video would play in background
                self.webengineview.setPage(self.solution_page)
                self.setFocus()  # otherwise focus would get lost to main window
                self.game.fail_current_task()
        elif self.webengineview.page() is self.solution_page:
            if key == Qt.Key_Escape or key == Qt.Key_Q:
                logging.debug("solution page is currently displayed, closing")
                # TODO key seems to get ignored because focus is on mainwindow, not here...
                # -> back to grid view on any key press
                # active_player should be None already
                self.main_window.update_ui_after_task()
                self.close()
        #elif modifiers == Qt.ControlModifier and key == Qt.Key_Z:
        #    logging.warning("Trying to undo the last action")
        #elif modifiers == Qt.ControlModifier and key == Qt.Key_Y:
        #    logging.warning("Trying to redo the last action")
        else:
            logging.debug(f"Ignoring: {key=}, {modifiers=}")
